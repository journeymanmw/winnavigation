﻿using System.Windows;
using NavTests.Nav;

namespace NavTests
{
    public partial class Page1 : INavigatableItem<object, object>
    {
        public Page1()
        {
            InitializeComponent();
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            App.Navigator.Navigate<Page2, int, int>(0, new NavigationCompletion<int>(_ => new CompletionInfo(NavigationCompletionPolicy.Remove)));
        }

        private async void OpenDialogLevel1(object sender, RoutedEventArgs e)
        {
            var nav = App.Navigator.Navigate<DialogLevel1, string, string>("arg");
            var result = await nav.Task;
            MessageBox.Show(result);
        }

        public void OnNavigated(object args, INavigationController<object> navigationController)
        {
        }
    }
}