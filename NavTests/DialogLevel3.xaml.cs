﻿using System.Windows;
using NavTests.Nav;

namespace NavTests
{
    public partial class DialogLevel3 : INavigatableItem<string, string>
    {
        private INavigationController<string> _navigationController;

        public DialogLevel3()
        {
            InitializeComponent();
        }

        private void ReturnResult(object sender, RoutedEventArgs e)
        {
            _navigationController.SetResult("Result from DialogLevel3");
        }

        public void OnNavigated(string args, INavigationController<string> navigationController)
        {
            _navigationController = navigationController;
        }
    }
}