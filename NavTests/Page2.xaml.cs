﻿using System.Windows;
using NavTests.Nav;

namespace NavTests
{
    public partial class Page2 : INavigatableItem<int, int>
    {
        private INavigationController<int> _navigationController;

        public Page2()
        {
            InitializeComponent();
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            _navigationController.SetResult(0);
        }

        public void OnNavigated(int args, INavigationController<int> navigationController)
        {
            _navigationController = navigationController;
        }
    }
}