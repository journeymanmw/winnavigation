using System.Threading.Tasks;

namespace NavTests.Nav
{
    public class ResultCompletionSource<T> : INavigationResult<T>
    {
        private readonly TaskCompletionSource<T> _tcs = new TaskCompletionSource<T>();

        public void SetResultReady(T result)
        {
            IsSuccess = true;
            _tcs.SetResult(result);
        }

        public void SetCancelled()
        {
            IsCancelled = true;
            _tcs.SetResult(default(T));
        }

        public bool IsCancelled { get; set; }
        public bool IsAborted { get; set; }
        public bool IsSuccess { get; set; }
        public Task<T> Task => _tcs.Task;
    }
}