using System.Threading.Tasks;

namespace NavTests.Nav
{
    public interface INavigationResult<T>
    {
        bool IsCancelled { get; }
        bool IsAborted { get; }
        bool IsSuccess { get; }
        Task<T> Task { get; }
    }
}