using System;

namespace NavTests.Nav
{
    public interface IPageTypeToUriMapper
    {
        Uri MapPage<T>();
    }
}