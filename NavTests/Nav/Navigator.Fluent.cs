namespace NavTests.Nav
{
    public partial class Navigator
    {
        public INavigationResult<TResult> Go<TView, TArgs, TResult>(To.IFullBuilder<TView, TArgs, TResult> builder, NavigationCompletion<TResult> navigationCompletion = null)
            where TView : INavigatableItem<TArgs, TResult>
        {
            return Navigate<TView, TArgs, TResult>(builder.Args, navigationCompletion);
        }
    }

    public static class To
    {
        public interface IArgsBuilder<TArgs>
        {
            TArgs Args { get; }
        }

        public interface IFullBuilder<TWindow, TArgs, TResult> : IWindowBuilder<TWindow, TResult>, IArgsBuilder<TArgs>
        {
        }

        public interface IWindowBuilder<TWindow, TResult>
        {
        }

        public static IWindowBuilder<TWindow, TResult> View<TWindow, TResult>()
        {
            return new Builder<TWindow, object, TResult>(null);
        }

        public static IWindowBuilder<TWindow, object> View<TWindow>()
        {
            return new Builder<TWindow, object, object>(null);
        }

        public static IFullBuilder<TWindow, TArgs, TResult> WithArgs<TWindow, TArgs, TResult>(this IWindowBuilder<TWindow, TResult> windowBuilder, TArgs args)
        {
            return new Builder<TWindow, TArgs, TResult>(args);
        }

        public class Builder<T, TArgs, TResult> : IFullBuilder<T, TArgs, TResult>
        {
            public Builder(TArgs args)
            {
                Args = args;
            }

            public TArgs Args { get; }
        }
    }
}