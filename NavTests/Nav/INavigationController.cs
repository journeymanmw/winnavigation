﻿using System;

namespace NavTests.Nav
{
    public interface INavigationController<TResult>
    {
        NavigationCompletion<TResult> Completion { get; }
        CompletionInfo SetResult(TResult result, NavigationCompletionPolicy nestedPolicy);
        void SetResult(TResult result);
        event Action<ResultSource<TResult>, CompletionInfo> ResultReady;
        event Action Destroyed;
    }
}