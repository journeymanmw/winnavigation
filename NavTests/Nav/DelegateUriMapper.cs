using System;
using System.Windows.Navigation;

namespace NavTests.Nav
{
    public class DelegateUriMapper : UriMapperBase
    {
        private readonly Func<Uri, Uri> _map;

        public DelegateUriMapper(Func<Uri, Uri> map)
        {
            _map = map;
        }

        public override Uri MapUri(Uri uri)
        {
            return _map(uri);
        }
    }
}