namespace NavTests.Nav
{
    public interface INavigatableItem<in TArgs> : INavigatableItem<TArgs, object>
    {
    }

    public interface INavigatableItem<in TArgs, TResult>
    {
        void OnNavigated(TArgs args, INavigationController<TResult> navigationController);
    }
}