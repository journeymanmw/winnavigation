namespace NavTests.Nav
{
    public enum NavigatedPolicy
    {
        DoNothing,
        RemovePrevious,
        ClearBackStack,
    }
}