using System;

namespace NavTests.Nav
{
    public class NavigationController<TResult> : INavigationController<TResult>
    {
        public NavigationCompletion<TResult> Completion { get; }

        public NavigationController(NavigationCompletion<TResult> completion)
        {
            Completion = completion;
        }

        public CompletionInfo SetResult(TResult result, NavigationCompletionPolicy nestedPolicy)
        {
            var source = new ResultSource<TResult>(result);
            var currentPolicy = Completion.GetPolicyFor(source);
            currentPolicy.Order.Add(nestedPolicy);
            //currentPolicy.Handled = true;
            SetResult(source, currentPolicy);
            return currentPolicy;
        }

        public void SetResult(TResult result)
        {
            var source = new ResultSource<TResult>(result);
            SetResult(source);
        }

        public void OnBack()
        {
            SetResult(new ResultSource<TResult>());
        }

        public event Action<ResultSource<TResult>, CompletionInfo> ResultReady;

        public void OnDestroyed() { Destroyed?.Invoke(); }

        public event Action Destroyed;

        private void SetResult(ResultSource<TResult> resultSource, CompletionInfo info = null)
        {
            var policy = info ?? Completion.GetPolicyFor(resultSource);
            ResultReady?.Invoke(resultSource, policy);
        }
    }
}