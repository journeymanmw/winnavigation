using System;
using System.ComponentModel;

namespace NavTests.Nav
{
    public partial class Navigator
    {
        private class NavigationOperation
        {
            private readonly Action<object> _onNavigated;
            private readonly Action<CancelEventArgs> _onNavigatedBack;
            private readonly Action _onDestroyed;

            public Uri Uri { get; }

            public NavigationOperation(Uri uri, Action<object> onNavigated, Action<CancelEventArgs> onNavigatedBack, Action onDestroyed)
            {
                _onNavigated = onNavigated;
                _onNavigatedBack = onNavigatedBack;
                _onDestroyed = onDestroyed;
                Uri = uri;
            }

            public void SetDestroyed()
            {
                _onDestroyed?.Invoke();
            }

            public void SetNavigated(object content)
            {
                _onNavigated?.Invoke(content);
            }

            public void SetNavigatedBack(CancelEventArgs cancelArgs)
            {
                _onNavigatedBack?.Invoke(cancelArgs);
            }

        }
    }
}