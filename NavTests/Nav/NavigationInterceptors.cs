using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Navigation;
using NavTests.Utils;

namespace NavTests.Nav
{
    public class NavigationInterceptors
    {

        public IList<INavigationInterceptor> Interceptors { get; }

        public UriMapperBase UriMapper { get; }

        public NavigationInterceptors(IEnumerable<INavigationInterceptor> interceptors)
        {
            Interceptors = interceptors.ToList();

            UriMapper = new DelegateUriMapper(inputUri => Interceptors.Aggregate(inputUri, (itermediateUri, interceptor) => interceptor.TryMapUriOrGetOriginal(itermediateUri)));
        }

        public Action GetFirstInterceptingAction(Uri uri)
        {
            foreach (var interceptor in Interceptors)
            {
                var action = interceptor.Intercept(uri);
                if (action != null)
                {
                    return action;
                }
            }

            return null;
        }
    }
}