using System;

namespace NavTests.Nav
{
    public interface INavigationInterceptor
    {
        bool TryMapUri(Uri uri, out Uri mappedUri);

        Action Intercept(Uri uri);
    }

    public static class NavigationInterceptorExtensions
    {
        public static Uri TryMapUriOrGetOriginal(this INavigationInterceptor interceptor, Uri uri)
        {
            Uri mapped;
            if (interceptor.TryMapUri(uri, out mapped))
            {
                return mapped;
            }

            return uri;
        }
    }
}