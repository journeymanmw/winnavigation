﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using NavTests.Utils;

namespace NavTests.Nav
{
    public partial class Navigator
    {
        private readonly PhoneApplicationFrame _frame;
        private readonly IPageTypeToUriMapper _pageTouriMapper;
        private readonly NavigationInterceptors _navigationInterceptors;
        private readonly IDictionary<Uri, NavigationOperation> _navigations = new Dictionary<Uri, NavigationOperation>();
        private readonly AsyncOperationQueue _queue;
        private readonly AsyncManualResetEvent _isNavigating = new AsyncManualResetEvent(true);
        private bool _isFirstNavigating = true;

        public Navigator(PhoneApplicationFrame frame, IPageTypeToUriMapper pageTouriMapper, NavigationInterceptors navigationInterceptors = null)
        {
            _queue = new AsyncOperationQueue(SynchronizationContext.Current);
            _frame = frame;
            _pageTouriMapper = pageTouriMapper;
            _navigationInterceptors = navigationInterceptors;

            _frame.JournalEntryRemoved += FrameOnJournalEntryRemoved;
            _frame.Navigated += CheckForResetNavigation;
            _frame.Navigated += FrameOnNavigated;
            _frame.Navigating += FrameOnNavigating;
            _frame.NavigationFailed += FrameOnNavigationFailed;
            _frame.NavigationStopped += FrameOnNavigationStopped;
            _frame.BackKeyPress += FrameOnBackKeyPress;
        }

        public event Action InitializedCleanly;
        public event Action<Uri> InitializedIntercepted;
        public bool CanGoBack => _frame.BackStack.Any();

        public INavigationResult<TResult> Navigate<TView, TArgs, TResult>(TArgs args, NavigationCompletion<TResult> navigationCompletion = null)
            where TView : INavigatableItem<TArgs, TResult>
        {
            navigationCompletion = navigationCompletion ?? new NavigationCompletion<TResult>(_ => new CompletionInfo(NavigationCompletionPolicy.Back));

            var uri = _pageTouriMapper.MapPage<TView>();
            var navResult = new ResultCompletionSource<TResult>();
            var navigationController = new NavigationController<TResult>(navigationCompletion);
            var navOperation = new NavigationOperation(
                uri,
                content =>
                {
                    navigationController.ResultReady += (result, completionInfo) =>
                    {
                        FinalizeNavigationWithResult(navResult, result, completionInfo);
                    };

                    if (navigationCompletion.NavigatedPolicy == NavigatedPolicy.RemovePrevious)
                    {
                        _frame.RemoveBackEntry();
                    }
                    else if (navigationCompletion.NavigatedPolicy == NavigatedPolicy.ClearBackStack)
                    {
                        ClearBackStack();
                    }

                    var page = (INavigatableItem<TArgs, TResult>)content;
                    page.OnNavigated(args, navigationController);
                },
                cancelArgs => navigationController.OnBack(),
                navigationController.OnDestroyed);

            _navigations.Add(uri, navOperation);
            ScheduleNavigationOperation(() => _frame.Navigate(uri));
            return navResult;
        }

        private void FinalizeNavigationWithResult<TResult>(ResultCompletionSource<TResult> resultCompletion, ResultSource<TResult> result, CompletionInfo completionInfo)
        {
            if (!completionInfo.Handled)
            {
                completionInfo.Handled = true;
                Action action = () =>
                {
                    var removeCount = completionInfo.Order.Count(x => x == NavigationCompletionPolicy.Remove);
                    var backCount = completionInfo.Order.Count(x => x == NavigationCompletionPolicy.Back);
                    if (backCount > 0)
                    {
                        backCount--;
                    }

                    var toRemove = removeCount + backCount;

                    for (int i = 0; i < toRemove; i++)
                    {
                        if (CanGoBack)
                        {
                            _frame.RemoveBackEntry();
                        }
                    }

                    if (CanGoBack)
                    {
                        _frame.GoBack();
                    }
                };

                if (result.IsResultSet)
                {
                    ScheduleNavigationOperation(action);
                }
                else
                {
                    action();
                }
            }

            ScheduleNavigationOperation(() =>
            {
                if (result.IsResultSet)
                {
                    resultCompletion.SetResultReady(result.Result);
                }
                else
                {
                    resultCompletion.SetCancelled();
                }
            });
        }

        private void FrameOnNavigated(object sender, NavigationEventArgs args)
        {
            _isNavigating.Set();

            if (_isFirstNavigating)
            {
                _isFirstNavigating = false;
                InitializedCleanly?.Invoke();
            }

            var navOperation = _navigations.TryGet(args.Uri);
            if (navOperation != null)
            {
                if (args.NavigationMode == NavigationMode.New)
                {
                    navOperation.SetNavigated(args.Content);
                }
            }
        }

        private void FrameOnJournalEntryRemoved(object sender, JournalEntryRemovedEventArgs args)
        {
            var nav = _navigations.TryGet(args.Entry.Source);
            if (nav != null)
            {
                nav.SetDestroyed();
                _navigations.Remove(args.Entry.Source);
            }
        }

        private void FrameOnNavigationStopped(object sender, NavigationEventArgs args)
        {
            _isNavigating.Set();
        }

        private void FrameOnNavigationFailed(object sender, NavigationFailedEventArgs args)
        {
            _isNavigating.Set();
        }

        private void FrameOnNavigating(object sender, NavigatingCancelEventArgs args)
        {
            Action interceptedAction = null;
            if (_navigationInterceptors != null && (interceptedAction = _navigationInterceptors.GetFirstInterceptingAction(args.Uri)) != null)
            {
                args.Cancel = true;

                if (_isFirstNavigating)
                {
                    _isFirstNavigating = false;
                    InitializedIntercepted?.Invoke(args.Uri);
                }

                ScheduleNavigationOperation(interceptedAction);
            }
            else
            {
                _isNavigating.Reset();
            }
        }

        private void FrameOnBackKeyPress(object sender, CancelEventArgs args)
        {
            var navOperation = _navigations.TryGet(_frame.Source);
            if (navOperation != null)
            {
                navOperation.SetNavigatedBack(args);
            }
        }

        private Task<T> ScheduleNavigationOperation<T>(Func<T> operation)
        {
            return _queue.Execute(async () =>
            {
                await _isNavigating.WaitAsync();
                return operation();
            });
        }

        private Task ScheduleNavigationOperation(Action operation)
        {
            return ScheduleNavigationOperation(() => { operation(); return 0; });
        }

        private void ClearBackStack()
        {
            while (_frame.RemoveBackEntry() != null)
            {
            }
        }

        private void CheckForResetNavigation(object sender, NavigationEventArgs e)
        {
            // If the app has received a 'reset' navigation, then we need to check
            // on the next navigation to see if the page stack should be reset
            if (e.NavigationMode == NavigationMode.Reset)
                _frame.Navigated += ClearBackStackAfterReset;
        }

        private void ClearBackStackAfterReset(object sender, NavigationEventArgs e)
        {
            // Unregister the event so it doesn't get called again
            _frame.Navigated -= ClearBackStackAfterReset;

            // Only clear the stack for 'new' (forward) and 'refresh' navigations
            if (e.NavigationMode != NavigationMode.New && e.NavigationMode != NavigationMode.Refresh)
                return;

            // For UI consistency, clear the entire page stack
            ClearBackStack();
        }
    }
}
