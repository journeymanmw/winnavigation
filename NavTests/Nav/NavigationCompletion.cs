using System;
using System.Collections.Generic;

namespace NavTests.Nav
{
    public struct ResultSource<TResult>
    {
        public ResultSource(TResult result)
        {
            Result = result;
            IsResultSet = true;
        }

        public TResult Result { get; }

        public bool IsResultSet { get; }
    }

    public class CompletionInfo
    {
        public CompletionInfo(NavigationCompletionPolicy initialPolicy)
        {
            Order.Add(initialPolicy);
        }

        public bool Handled { get; set; }

        public IList<NavigationCompletionPolicy> Order { get; } = new List<NavigationCompletionPolicy>();
    }

    public class NavigationCompletion<TResult>
    {
        private readonly Func<ResultSource<TResult>, CompletionInfo> _onResultProvidedPolicy;

        public NavigationCompletion(Func<ResultSource<TResult>, CompletionInfo> onResultProvidedPolicy, NavigatedPolicy navigatedPolicy = NavigatedPolicy.DoNothing)
        {
            _onResultProvidedPolicy = onResultProvidedPolicy;
            NavigatedPolicy = navigatedPolicy;
        }

        public NavigatedPolicy NavigatedPolicy { get; }

        public CompletionInfo GetPolicyFor(ResultSource<TResult> result)
        {
            return _onResultProvidedPolicy(result);
        }
    }
}