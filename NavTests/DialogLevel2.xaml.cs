﻿using System.Windows;
using NavTests.Nav;

namespace NavTests
{
    public partial class DialogLevel2 : INavigatableItem<string, string>
    {
        private INavigationController<string> _navigationController;
        private string _arg;

        public DialogLevel2()
        {
            InitializeComponent();
        }

        private void ReturnResult(object sender, RoutedEventArgs e)
        {
            _navigationController.SetResult("Result from DialogLevel2");
        }

        public void OnNavigated(string args, INavigationController<string> navigationController)
        {
            _arg = args;
            _navigationController = navigationController;
        }

        private void GotoDialogLevel3(object sender, RoutedEventArgs e)
        {
            App.Navigator.Navigate<DialogLevel3, string, string>("arg2", new NavigationCompletion<string>(result =>
            {
                return _navigationController.SetResult(result.Result + " + result from DialogLevel2", NavigationCompletionPolicy.Remove);
            }));
        }
    }
}