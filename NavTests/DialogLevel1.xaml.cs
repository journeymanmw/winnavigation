﻿using System.Windows;
using NavTests.Nav;

namespace NavTests
{
    public partial class DialogLevel1 : INavigatableItem<string, string>
    {
        private string _arg;
        private INavigationController<string> _navigationController;

        public DialogLevel1()
        {
            InitializeComponent();
        }

        private void GotoDialogLevel2(object sender, RoutedEventArgs e)
        {
            App.Navigator.Navigate<DialogLevel2, string, string>("arg1", new NavigationCompletion<string>(result =>
            {
                return _navigationController.SetResult(result.Result + " + result from DialogLevel1", NavigationCompletionPolicy.Remove);
            }));
        }

        private void ReturnResult(object sender, RoutedEventArgs e)
        {
            _navigationController.SetResult("Result from DialogLevel1");
        }

        public void OnNavigated(string args, INavigationController<string> navigationController)
        {
            _arg = args;
            _navigationController = navigationController;
        }
    }
}