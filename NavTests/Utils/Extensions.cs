﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace NavTests
{
    public static class Extensions
    {
        public class QueryParams : Dictionary<string, string> { }

        public static TOut TryGet<TIn, TOut>(this IDictionary<TIn, TOut> dictionary, TIn key, TOut defaultValue = default(TOut))
        {
            var result = defaultValue;
            if (dictionary.TryGetValue(key, out result))
            {
                return result;
            }

            return defaultValue;
        }

        public static string ToUrl(string path, QueryParams query)
        {
            return $"{path}?{string.Join("&", query.Select(x => $"{x.Key}={x.Value}"))}";
        }

        public static IDictionary<string, string> ParseQueryString(this Uri uri, char divider = '?')
        {
            var toReturn = new Dictionary<string, string>();
            if (uri == null)
            {
                return toReturn;
            }

            var simplePath = uri.OriginalString; //SS: AbsoluteUri is escaped
            var question = simplePath.IndexOf(divider);
            if (question < 0)
            {
                return toReturn;
            }

            if (question >= (simplePath.Length - 1))
            {
                return toReturn;
            }

            var parameters = simplePath.Substring(question + 1);

            foreach (var vp in Regex.Split(parameters, "&"))
            {
                string[] singlePair = Regex.Split(vp, "=");
                if (singlePair.Length == 2)
                {
                    toReturn[singlePair[0]] = Uri.UnescapeDataString(singlePair[1]);
                }
                else
                {
                    toReturn[singlePair[0]] = string.Empty;
                }
            }

            return toReturn;
        }
        public static string ToQueryString(this IDictionary<string, string> nvc, bool encodeValue = true)
        {
            if (encodeValue)
            {
                var array = (from key in nvc.Keys
                             let value = nvc[key]
                             select $"{WebUtility.UrlEncode(key)}={WebUtility.UrlEncode(value)}")
                    .ToArray();
                return "?" + string.Join("&", array);
            }
            else
            {
                var array = (from key in nvc.Keys
                             let value = nvc[key]
                             select $"{WebUtility.UrlEncode(key)}={value}")
                    .ToArray();
                return "?" + string.Join("&", array);
            }
        }
    }
}
