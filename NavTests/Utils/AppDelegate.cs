﻿using System;
using System.Collections.Generic;
using Windows.Networking.PushNotifications;
using Microsoft.Phone.Controls;
using NavTests.Nav;

namespace NavTests.Utils
{
    public class EmptyNavigationHandler : INavigationInterceptor
    {
        public bool TryMapUri(Uri uri, out Uri mappedUri)
        {
            if (string.IsNullOrEmpty(uri.OriginalString))
            {
                mappedUri = new Uri("/MainPage.xaml", UriKind.Relative);
                return true;
            }

            mappedUri = null;
            return false;
        }

        public Action Intercept(Uri uri)
        {
            if (uri.OriginalString.ToLower().StartsWith("/some_start_uri"))
            {
                return AppDelegate.OnNormalStart;
            }

            return null;
        }
    }

    public class PushHandler : INavigationInterceptor
    {
        public event Action<Uri> Intercepted;

        public bool TryMapUri(Uri uri, out Uri mappedUri)
        {
            if (uri.OriginalString.ToLower().Contains("/push"))
            {
                var query = uri.ParseQueryString();
                mappedUri = new Uri($"/MainPage.xaml{query.ToQueryString()}", UriKind.Relative);
                return true;
            }

            mappedUri = null;
            return false;
        }

        public Action Intercept(Uri uri)
        {
            if (uri.OriginalString.ToLower().Contains("/push"))
            {
                Intercepted?.Invoke(uri);
                return () => AppDelegate.Navigator.Navigate<DialogLevel1, string, string>("push");
            }

            return null;
        }
    }

    public class PageUriResolver : IPageTypeToUriMapper
    {
        public Uri MapPage<T>()
        {
            return new Uri($"/{typeof(T).Name}.xaml", UriKind.Relative);
        }
    }

    public class AppDelegate
    {
        public static Navigator Navigator { get; set; }

        public static void OnNormalStart()
        {
            Navigator.Navigate<Page1, object, object>(null, new NavigationCompletion<object>(_ => new CompletionInfo(NavigationCompletionPolicy.Back), NavigatedPolicy.RemovePrevious));
        }

        public void OnCreated(PhoneApplicationFrame rootFrame)
        {
            var pushInterceptor = new PushHandler();
            var emptyInterceptor = new EmptyNavigationHandler();
            var interceptors = new NavigationInterceptors(new INavigationInterceptor[] { pushInterceptor, emptyInterceptor });
            rootFrame.UriMapper = interceptors.UriMapper;
            Navigator = new Navigator(rootFrame, new PageUriResolver(), interceptors);

            Navigator.InitializedCleanly += OnNormalStart;
            Navigator.InitializedIntercepted += uri =>
            {
            };

            PushNotificationChannelManager.CreatePushNotificationChannelForApplicationAsync();
        }
    }
}
