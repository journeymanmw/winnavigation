﻿using System;
using System.Windows;
using System.Windows.Data;

namespace NavTests
{
    public class PropertyChangedListener<T> : DependencyObject
    {
        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register("Value", typeof(T), typeof(PropertyChangedListener<T>), new PropertyMetadata(default(T), OnValueChanged));

        public PropertyChangedListener(string path, DependencyObject source)
        {
            var binding = new Binding(path) { Source = source };
            BindingOperations.SetBinding(this, ValueProperty, binding);
        }

        public event EventHandler<DependencyPropertyChangedEventArgs> ValueChanged;

        public T Value
        {
            get { return (T)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        private static void OnValueChanged(DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            var listener = (PropertyChangedListener<T>)s;
            listener.RaiseValueChanged(e);
        }

        private void RaiseValueChanged(DependencyPropertyChangedEventArgs args)
        {
            ValueChanged?.Invoke(this, args);
        }
    }
}